#!/bin/bash


# Configuration automatique du Proxy sur Linux

# Couleurs
COLTITRE="\033[1;35m"   # Rose
COLPARTIE="\033[1;34m"  # Bleu
COLTXT="\033[0;37m"     # Gris
COLCHOIX="\033[1;33m"   # Jaune
COLDEFAUT="\033[0;33m"  # Brun-jaune
COLSAISIE="\033[1;32m"  # Vert
COLCMD="\033[1;37m"     # Blanc
COLSTOP="\033[1;31m"  # Rouge
COLINFO="\033[0;36m"    # Cyan


STOPPER()
{
        echo -e "$COLSTOP"
        echo -e "Vous avez décidé de ne pas continuer à configurer le proxy. Vous pouvez reprendre la procédure quand vous voulez."
	echo -e "$1"
        echo -e "$COLTXT"
        exit 1
}



POURSUIVRE()
{
        REPONSE=""
        while [ "$REPONSE" != "o" -a "$REPONSE" != "O" -a "$REPONSE" != "n" ]
        do
          echo -e "$COLTXT"
	  echo -e "Peut-on poursuivre (o par défaut) ? (${COLCHOIX}o/n${COLTXT}) $COLSAISIE\c"
	  read REPONSE
          if [ -z "$REPONSE" ]; then
	     REPONSE="o"
	  fi
        done
        if [ "$REPONSE" != "o" -a "$REPONSE" != "O" ]; then
	   STOPPER
	fi
}



echo -e "$COLTXT"
echo -e "Passez-vous par un proxy pour vous connecter à internet (n par défaut) ? (${COLCHOIX}o/n${COLTXT}) ${COLSAISIE}\c "
read REPONSE
if [ -z "$REPONSE" ]; then
   REPONSE="n"
fi

if [ "$REPONSE" = "o" ]; then
   echo -e "$COLTXT"
   echo -e "Saisissez l'adresse du proxy : [${COLDEFAUT}${http_proxy}${COLTXT}] ${COLSAISIE}\c"
   echo -e "(Validez l'adresse du proxy proposée sinon saisir ip-proxy:port) :"
   read ADRESSE_PROXY
   if [ -z "$ADRESSE_PROXY" ]; then
      ADRESSE_PROXY="$http_proxy"
   fi
   if [ -z "$http_proxy" ]; then
      export http_proxy="$ADRESSE_PROXY"
      export https_proxy="$ADRESSE_PROXY"
   fi 
   echo -e "$COLTXT"
   echo -e "Saisissez les hôtes à ignorer par le proxy. $COLSAISIE\c"
   echo -e "(Saisir les hôtes à ignorer séparés par une virgule (les caractères spéciaux comme \".\" ou \"*\" sont acceptés) :"
   read NO_PROXY
fi

if [ "$REPONSE" = "n" ]; then
   echo -e "$COLTXT"
   echo -e "Aucun proxy n'est configuré sur le système."
   echo -e "Les paramètres du proxy, s'ils existent, seront supprimés."
   else
     echo -e "$COLINFO"
     echo -e "Vous vous apprêtez à utiliser les paramètres suivants:"
     echo -e "Proxy :	$ADRESSE_PROXY"
     echo -e "No Proxy :  $NO_PROXY"
     echo -e "$COLCMD"
fi

POURSUIVRE

# Création du fichier config.json s'il n'existe pas
if [ ! -e ~/.docker ]; then
       mkdir ~/.docker
       echo -e "$COLDEFAUT"
fi

if [ ! -e ~/.docker/config.json ]; then
       echo -e "Création d'un fichier vide config.json"
       echo -e "$COLCMD\c"
       echo "" > ~/.docker/config.json
       chown -R $USER:docker ~/.docker
       chmod g+rw ~/.docker/config.json
fi

#Configuration du proxy pour GIT et pour Docker

if [ "$ADRESSE_PROXY" != "" ]; then
   echo -e "$COLDEFAUT"
   echo -e "Congiguration de GIT pour le proxy"
   echo -e "$COLCMD\c"
   git config --global http.proxy $ADRESSE_PROXY
   if [ ! -d "/etc/systemd/system/docker.service.d" ]; then
          mkdir /etc/systemd/system/docker.service.d
   fi
   echo -e "$COLDEFAUT"
   echo "Ajout des variables d'environnement à systemd (/etc/systemd/system/docker.service.d/http-proxy.conf)"
   echo -e "$COLCMD\c"
   echo "[Service]" > /etc/systemd/system/docker.service.d/http-proxy.conf
   echo "Environment=\"HTTP_PROXY=http://$ADRESSE_PROXY\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
   echo "Environment=\"HTTPS_PROXY=http://$ADRESSE_PROXY\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
   echo "Environment=\"NO_PROXY=$NO_PROXY\"" >> /etc/systemd/system/docker.service.d/http-proxy.conf
   echo ""
   echo -e "Redémarrage de Docker"
   systemctl daemon-reload
   systemctl restart docker
   echo -e "$COLDEFAUT"
   echo -e "Ajout des paramètres du Proxy dans ~/.docker/config.json après sauvegarde du fichier d'origine et du fichier actuel."
   echo -e "$COLCMD\c"
   echo "Ajout des paramètres du proxy $ADRESSE_PROXY et $NO_PROXY"
   echo ""
   # Sauvegarde du fichier config.json d'origine si ce n'est déjà fait
   if [ ! -e ~/.docker/config.json.ori ]; then
       cp ~/.docker/config.json ~/.docker/config.json.ori
   fi
   # Sauvegarde du fichier actuel
   cp ~/.docker/config.json ~/.docker/config.json.sauv
   # Ajout des paramètres du proxy au fichier config.json
   if [ `sed -n '$=' /root/.docker/config.json` = 1 ]; then
   sed -i "1i \ {\n\t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n  }\n\t}\n}" ~/.docker/config.json
   else
    sed -i.bak "/\"proxies\": {/,10d" ~/.docker/config.json
    if [ `sed -n '$=' /root/.docker/config.json` = 2 ]; then
       sed -i "1a \ \t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n  }\n\t}\n}" ~/.docker/config.json
       else
         sed -i.bak "1a \ \t\"proxies\": {\n\t  \"default\":\n\t  {\n\t\t\"httpProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"httpsProxy\": \"http:\/\/$ADRESSE_PROXY\",\n\t\t\"noProxy\": \"$NO_PROXY\"\n\t  }\n\t},\n" ~/.docker/config.json
       fi
    fi
     else        
        echo -e "$COLCMD"	
        git config --global --unset http.proxy
        if [ -f "/etc/systemd/system/docker.service.d/http-proxy.conf" ]; then
           rm -f /etc/systemd/system/docker.service.d/http-proxy.conf
           systemctl daemon-reload
           systemctl restart docker
        fi
        # Sauvegarde du fichier actuel
        cp ~/.docker/config.json ~/.docker/config.json.sauv
        # Suppression des paramètres du proxy du fichier config.json
        sed -i.bak "/\"proxies\": {/,9d" ~/.docker/config.json
        if [ `sed -n '$=' /root/.docker/config.json` = 1 ]; then
                echo "Le fichier config.json est supprimé"
                rm -rf ~/.docker/config.json
        fi
        echo -e "$COLINFO"
        echo -e "Aucun proxy n'est configuré sur le système."
        echo -e "Les paramètres du proxy, s'ils existaient, ont été supprimés."
fi

